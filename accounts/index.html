<!DOCTYPE html>
<html lang="en">

  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Accounts</title>
  <meta name="description" content="MREST Documentation is licensed under CC BY 4.0. © 2015 Deginner.">

  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=1">

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/solo.css">
  
  <link rel="stylesheet" href="/css/prism.css">
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  <link rel="canonical" href="http://mrest.deginner.com/doc//accounts/">
  <link rel="alternate" type="application/rss+xml" title="MREST Documentation" href="http://mrest.deginner.com/doc//feed.xml" />
</head>


  <body>

    <header class="site-header">
  <div class="wrapper">
    <a class="site-title" href="/">MREST Documentation</a>
  </div>
</header>


    <div class="page-content">
      <div class="container">
        <div class="improve">
  <a href="https://bitbucket.org/deginner/mrest-doc/src/master/accounts.md" target="_blank" title="Edit this page">
    <i class="fa fa-pencil"></i>
  </a>
</div>


        <article>
          <div class="post">

  <header class="post-header">
    <h2 class="post-title" id="guide-intro">Accounts</h2>
    <!--<p class="post-meta"></p>-->
  </header>

  <article class="post-content">
    <h2 id="summary">Summary</h2>
<p>MREST applications often have user accounts, and require <a href="/authentication">authentication</a> for restricted resources. While this isn’t needed in all situations, there are some best practices to follow if you choose to make use of accounts.</p>

<h2 id="default-user-schema">Default User Schema</h2>
<p>The official <a href="https://bitbucket.org/deginner/flask-mrest">Flask MREST</a> server comes with a default User schema, which is used in the example app. This default User is the second simplest example of how an MREST user model could be structured. It has only two properties: username and pubhash.</p>

<div class="highlight"><pre><code class="language-json" data-lang="json">{
      &quot;$schema&quot;: &quot;http://json-schema.org/draft-04/schema#&quot;, 
      &quot;description&quot;: &quot;model for an api user or item user&quot;, 
      &quot;properties&quot;: {
        &quot;pubhash&quot;: {
          &quot;maxLength&quot;: 36, 
          &quot;type&quot;: &quot;string&quot;
        }, 
        &quot;username&quot;: {
          &quot;maxLength&quot;: 36, 
          &quot;type&quot;: &quot;string&quot;
        }
      }, 
      &quot;required&quot;: [
        &quot;pubhash&quot;, 
        &quot;username&quot;
      ], 
      &quot;routes&quot;: {
        &quot;/&quot;: {
          &quot;POST&quot;: [
            &quot;pubhash&quot;
          ]
        }, 
        &quot;/:id&quot;: {
          &quot;GET&quot;: [
            &quot;authenticate&quot;
          ]
        }
      }, 
      &quot;title&quot;: &quot;UserSA&quot;, 
      &quot;type&quot;: &quot;object&quot;
    }
  }</code></pre></div>

<p>Add more attributes as your needs fit, but always consider interoperability. Clients will not necessarily understand additional property requirements. Consider using additional objects owned by the user.</p>

<h3 id="ownership-and-authentication">Ownership and Authentication</h3>
<p>By adding the user public key hash as a foreign key to other models, you can implement ownership and require authentication.</p>

<p>It is up to the application developer to implement specific authentication checks, the standard is to only allow access to ‘authenticate’ routes if the signer’s public key is referenced by the object.</p>

<div class="highlight"><pre><code class="language-python" data-lang="python">class CoinSA(SABase):
    &quot;&quot;&quot;model for coin&quot;&quot;&quot;
    __tablename__ = &quot;coin&quot;

    id = sa.Column(sa.Integer, primary_key=True, doc=&quot;primary key&quot;)
    metal = sa.Column(sa.String(255), nullable=False)
    mint = sa.Column(sa.String(255), nullable=False)

    # the owner of the coin. only authenticate if this user has signed.
    user_pubhash = sa.Column(sa.String(120), sa.ForeignKey(&#39;user.pubhash&#39;), nullable=False)
    user = orm.relationship(&quot;UserSA&quot;)</code></pre></div>

<h2 id="registration">Registration</h2>
<p>To register for a server, call POST to the ‘user_model’ specified in the server info. Typically, this will require your client’s public key hash, and a username unique to that server. You could use the first bits of your bitcoin address, if you are lazy.</p>

<div class="highlight"><pre><code class="language-python" data-lang="python">self.post(&#39;user&#39;, {&quot;pubhash&quot;: self.pubhash, &quot;username&quot;: self.pubhash[0:8]})</code></pre></div>

<h2 id="server-discovery">Server Discovery</h2>
<p>By visiting the <a href="/routes">Info route</a> on any MREST server, a client can learn all it needs to know to use that server. In this way, a client could utilize many servers with different schemas and purposes throughout its lifecycle.</p>

<h3 id="steps">Steps</h3>
<p>A client should follow the same steps each time it discovers a server it wishes to use.</p>

<ol>
  <li>Visit the server’s / route to learn about the server privacy, authentication, registration, and other policies.</li>
  <li>Load the server’s routes and permissions into the client for request formatting.</li>
  <li>Load the server’s public keyring, if you trust it.</li>
  <li>Load the json schemas.</li>
  <li>Register by creating an instance (POST) of the user model supplying your client’s bitcoin public key in the request header.</li>
  <li>Cache the schemas, keyring, routes, url, and other configuration info for next time</li>
</ol>

<p>The above steps can be completed manually by simply providing them in the configuration argument to your client constructor. This should be done for dedicated or highly sensitive clients. In the case where <a href="/authentication/#additional-signers">mulptiple signers</a> are used, all of the keys should be cached in your keyring and passed in at client initialization.</p>

<h3 id="rediscovery">Rediscovery</h3>
<p>After the initial discovery, the cached configuration should be checked on client initialization. If any of the important data, particularly the server’s signing key have changed, an exception should be raised.</p>

<h3 id="use-case">Use case</h3>
<p>Lets say there is an application named Happy. Happy lives in a Lamassu BTM, and has access to an MREST client. Every day people walk up to Happy’s machine and he helps them find and buy goods and services from MREST servers.</p>

<p>Every night Happy checks the MREST forums and registries to see if any new MREST servers are available. When a new one is discovered, Happy calls their / route, and compares their schemas with his needs. Do they have a catalog of items for sale? Do they accept Bitcoin payments in his programmed checkout process? If so, Happy begins communicating with the server, requesting their catalog, entering orders for users, etc..</p>

<p>After some time, Happy notices that lots of users are searching for a particular item that he does not have in his catalog. Something called a “Jimmy Choo”. Happy has no idea what this Jimmy Choo thing is, but clearly users want it!</p>

<p>Some months later, Happy discovers a new store in his nightly update: choo247.com. When he gets their schemas, he sees that they have a catalog. Sure enough, they have a number items in the “shoe” category that match the brand name “Jimmy Choo”. Happy knows somes users that will like this, so he promptly adds the items to the catalog and emails the users. The next day, a line of customers show up to buy the latest shoes. His plan succeeded!</p>

  </article>

</div>

        </article>

        
        <div class="top-pages">
          <hr/>
          <h2>Contents</h2>
          <ul class="post-list">
  
  
  
    
  
    
  
    
    <li>
      
      <a class="post-link" href="/index.html">Introduction</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/data-types/">Data Types</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/routes/">Routes</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/requests/">Requests</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/responses/">Responses</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/authentication/">Authentication</a>
      
    </li>
    
  
    
    <li>
      
      → Accounts
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/clients/">Clients</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/servers/">Servers</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/contribute/intro.html">Contributing</a>
      
    </li>
    
  
</ul>

        </div>
        

        
      </div>

      
      <div class="left-menu">
        <div id="toc" class="toc left-menu-wrapper"></div>
        <div class="top-pages">
          <hr id="hr-toc" />
          <div class="left-menu-wrapper">
            <i>Contents</i>
            <ul class="post-list">
  
  
  
    
  
    
  
    
    <li>
      
      <a class="post-link" href="/index.html">Introduction</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/data-types/">Data Types</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/routes/">Routes</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/requests/">Requests</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/responses/">Responses</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/authentication/">Authentication</a>
      
    </li>
    
  
    
    <li>
      
      → Accounts
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/clients/">Clients</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/servers/">Servers</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/contribute/intro.html">Contributing</a>
      
    </li>
    
  
</ul>

          </div>
        </div>
      </div>
      
    </div>

    <footer class="site-footer">
  <div class="wrapper">
    <p class="text">MREST Documentation is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.<br/> © 2015 Deginner.</p>
  </div>
</footer>

    
<script src="/js/prism.js"></script>


<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/toc.js"></script>
<script src="/js/jquery.waypoints.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $(window).bind('scroll', function () {
      $('.left-menu').toggleClass('left-menu-fixed', $(window).scrollTop() > 132);
    });

    $('#toc').toc({minimumHeaders: 2, noBackToTopLinks: true});
    var elements = $('h2, h3, h4');
    var elemCount = 0;

    /* Set up way points. */
    elements.each(function() {
      if (!this.id) {
        return;
      }
      elemCount++;

      var elem = this.id;
      var wp = new Waypoint({
        element: $('#' + elem),
        handler: function() {
          $('#toc a').removeClass('waypoint');
          $('#toc a[href=#' + elem + ']').toggleClass('waypoint');
        },
        offset: 125
      });
    });

    if (elemCount < 2) {
      $('#hr-toc').hide();
    }

  });
</script>


  </body>

</html>
