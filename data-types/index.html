<!DOCTYPE html>
<html lang="en">

  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Data Types</title>
  <meta name="description" content="MREST Documentation is licensed under CC BY 4.0. © 2015 Deginner.">

  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?v=1">

  <link rel="stylesheet" href="/css/normalize.css">
  <link rel="stylesheet" href="/css/solo.css">
  
  <link rel="stylesheet" href="/css/prism.css">
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  <link rel="canonical" href="http://mrest.deginner.com/doc//data-types/">
  <link rel="alternate" type="application/rss+xml" title="MREST Documentation" href="http://mrest.deginner.com/doc//feed.xml" />
</head>


  <body>

    <header class="site-header">
  <div class="wrapper">
    <a class="site-title" href="/">MREST Documentation</a>
  </div>
</header>


    <div class="page-content">
      <div class="container">
        <div class="improve">
  <a href="https://bitbucket.org/deginner/mrest-doc/src/master/data_types.md" target="_blank" title="Edit this page">
    <i class="fa fa-pencil"></i>
  </a>
</div>


        <article>
          <div class="post">

  <header class="post-header">
    <h2 class="post-title" id="guide-intro">Data Types</h2>
    <!--<p class="post-meta"></p>-->
  </header>

  <article class="post-content">
    <h2 id="json-schemas">JSON Schemas</h2>
<p>MREST uses JSON schemas to define models, routes, permissions, and signing keys.</p>

<p>The schemas each contain two custom attributes: routes and signers.</p>

<table>
  <thead>
    <tr>
      <th>Attribute</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>routes</td>
      <td>An object showing routes and permissions for the model. See ‘Routes &amp; Permissions’ subsection.</td>
    </tr>
    <tr>
      <td>signers</td>
      <td>Specific signers which are required for authenticating an item. (optional)</td>
    </tr>
  </tbody>
</table>

<p>For example, lets say you wanted to build an API to manage a collection of coins. Each coin has two required properties: metal &amp; mint. This model can be represented as the following json schema:</p>

<div class="highlight"><pre><code class="language-json" data-lang="json">{
      &quot;$schema&quot;: &quot;http://json-schema.org/draft-04/schema#&quot;, 
      &quot;description&quot;: &quot;model for coin&quot;, 
      &quot;properties&quot;: {
        &quot;metal&quot;: {
          &quot;maxLength&quot;: 255, 
          &quot;type&quot;: &quot;string&quot;
        }, 
        &quot;mint&quot;: {
          &quot;maxLength&quot;: 255, 
          &quot;type&quot;: &quot;string&quot;
        }
      }, 
      &quot;required&quot;: [
        &quot;metal&quot;, 
        &quot;mint&quot;
      ], 
      &quot;routes&quot;: {
        &quot;/&quot;: {
          &quot;GET&quot;: [], 
          &quot;POST&quot;: [
            &quot;authenticate&quot;
          ]
        }, 
        &quot;/:id&quot;: {
          &quot;DELETE&quot;: [
            &quot;authenticate&quot;
          ], 
          &quot;GET&quot;: [], 
          &quot;PUT&quot;: [
            &quot;authenticate&quot;
          ]
        }
      }, 
      &quot;title&quot;: &quot;CoinSA&quot;, 
      &quot;type&quot;: &quot;object&quot;
}</code></pre></div>

<p>This schema would be given a name (i.e. ‘coin’) and distributed to both the MREST server and clients. Both would know that any time they are sending or receiving a coin, they should validate it against the schema. The custom attributes routes and signers indicate which routes and authentication the client should expect for the model.</p>

<h3 id="routes--permissions">Routes &amp; Permissions</h3>
<p>These indicate the authentication rules for the model’s routes. The “/” and “/:id” subsets indicate whether the restriction applies to the /:model/ route or to the /:model/:id route. For more information on the permissions, see <a href="/routes/#route-permissions">Route Permissions</a></p>

<h3 id="signers">Signers</h3>
<p>One or more <a href="/authentication/#signing">authentication signatures</a> will be included in MREST requests for authenticated routes. When specific signers are included in the schema for a model, the client will check each incoming item for signatures by each of those upon receipt. This comes in useful for multi-tier architectures where many backend servers work together to satisfy a request.</p>

<h2 id="mrest-messages">MREST Messages</h2>
<p>Raw MREST messages are any json that satisfies one of the json schemas. For the example schema above, the following would be a valid raw MREST message:</p>

<div class="highlight"><pre><code class="language-json" data-lang="json">{&quot;mint&quot;: &quot;Perth&quot;, &quot;metal&quot;: &quot;AU&quot;}</code></pre></div>

<h3 id="signed-format">Signed Format</h3>
<p>To ensure consistent inputs for hashing, signed messages are base64 encoded then wrapped in another JSON object before sending.</p>

<ol>
  <li>Base64 encode the raw (json encoded) message.</li>
  <li>Create a json object with one attribute “data” whose value is the output of step 1.</li>
</ol>

<p>The base64 encoded message is used to construct the sign string during <a href="/authentication/#signing">signing</a>.</p>

<p>For the example raw message above, the signed message body would look like this:</p>

<div class="highlight"><pre><code class="language-json" data-lang="json">{&quot;data&quot;: &quot;eyJtaW50IjogIlBlcnRoIiwgIm1ldGFsIjogIkFVIn0=&quot;}</code></pre></div>

<p>The norm is to send headers separate from the message body. This is the format used by the official HTTP clients.</p>

<h3 id="compact-signed-format">Compact Signed Format</h3>
<p>The compact MREST message format includes the headers in the message body. This format can be used where custom headers are not supported or easily available, such as for AMQP or the official SockJS server.</p>

<div class="highlight"><pre><code class="language-json" data-lang="json">{
    &quot;headers&quot;: {
        &quot;x-mrest-time&quot;: &quot;1439854057.62&quot;,
        &quot;x-mrest-pubhash&quot;: &quot;1GnsHrVdB4khKzHbiRyYfrrn5E9MHutyCQ&quot;,
        &quot;x-mrest-sign&quot;: &quot;IHqn5RXd8R79Fp9r9Dm9n5DdEZmG0FbbcIqFE9BmvyntXdI6YWZtyc6eIj/FV4SBqYXAYDR47tHU9LU5XSN4lao=&quot;
    },
    &quot;data&quot;: &quot;eyJtaW50IjogIlBlcnRoIiwgIm1ldGFsIjogIkFVIn0=&quot;,
    &quot;method&quot;: &quot;POST&quot;
}</code></pre></div>

  </article>

</div>

        </article>

        
        <div class="top-pages">
          <hr/>
          <h2>Contents</h2>
          <ul class="post-list">
  
  
  
    
  
    
  
    
    <li>
      
      <a class="post-link" href="/index.html">Introduction</a>
      
    </li>
    
  
    
    <li>
      
      → Data Types
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/routes/">Routes</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/requests/">Requests</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/responses/">Responses</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/authentication/">Authentication</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/accounts/">Accounts</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/clients/">Clients</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/servers/">Servers</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/contribute/intro.html">Contributing</a>
      
    </li>
    
  
</ul>

        </div>
        

        
      </div>

      
      <div class="left-menu">
        <div id="toc" class="toc left-menu-wrapper"></div>
        <div class="top-pages">
          <hr id="hr-toc" />
          <div class="left-menu-wrapper">
            <i>Contents</i>
            <ul class="post-list">
  
  
  
    
  
    
  
    
    <li>
      
      <a class="post-link" href="/index.html">Introduction</a>
      
    </li>
    
  
    
    <li>
      
      → Data Types
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/routes/">Routes</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/requests/">Requests</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/responses/">Responses</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/authentication/">Authentication</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/accounts/">Accounts</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/clients/">Clients</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/servers/">Servers</a>
      
    </li>
    
  
    
    <li>
      
      <a class="post-link" href="/contribute/intro.html">Contributing</a>
      
    </li>
    
  
</ul>

          </div>
        </div>
      </div>
      
    </div>

    <footer class="site-footer">
  <div class="wrapper">
    <p class="text">MREST Documentation is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a>.<br/> © 2015 Deginner.</p>
  </div>
</footer>

    
<script src="/js/prism.js"></script>


<script src="/js/jquery-1.11.3.min.js"></script>
<script src="/js/toc.js"></script>
<script src="/js/jquery.waypoints.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {

    $(window).bind('scroll', function () {
      $('.left-menu').toggleClass('left-menu-fixed', $(window).scrollTop() > 132);
    });

    $('#toc').toc({minimumHeaders: 2, noBackToTopLinks: true});
    var elements = $('h2, h3, h4');
    var elemCount = 0;

    /* Set up way points. */
    elements.each(function() {
      if (!this.id) {
        return;
      }
      elemCount++;

      var elem = this.id;
      var wp = new Waypoint({
        element: $('#' + elem),
        handler: function() {
          $('#toc a').removeClass('waypoint');
          $('#toc a[href=#' + elem + ']').toggleClass('waypoint');
        },
        offset: 125
      });
    });

    if (elemCount < 2) {
      $('#hr-toc').hide();
    }

  });
</script>


  </body>

</html>
